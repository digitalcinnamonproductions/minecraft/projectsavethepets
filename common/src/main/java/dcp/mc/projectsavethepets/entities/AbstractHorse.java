package dcp.mc.projectsavethepets.entities;

import dcp.mc.projectsavethepets.apis.NoteGeneratorApi;
import dcp.mc.projectsavethepets.apis.OwnershipApi;
import dcp.mc.projectsavethepets.apis.PetApi;
import java.util.UUID;
import net.minecraft.entity.passive.AbstractHorseEntity;
import net.minecraft.nbt.NbtCompound;
import org.jetbrains.annotations.NotNull;

public final class AbstractHorse implements NoteGeneratorApi<AbstractHorseEntity>, OwnershipApi<AbstractHorseEntity>, PetApi<AbstractHorseEntity> {
    public static final AbstractHorse INSTANCE = new AbstractHorse();

    private AbstractHorse() {
    }

    @Override
    public void setupNbt(@NotNull AbstractHorseEntity entity, @NotNull NbtCompound nbt) {
        nbt.putBoolean("EatingHaystack", false);
        nbt.remove("Bred");
        nbt.remove("SaddleItem");
    }

    @Override
    public @NotNull Class<AbstractHorseEntity> type() {
        return AbstractHorseEntity.class;
    }

    @Override
    public boolean isPet(@NotNull AbstractHorseEntity entity) {
        return entity.isTame();
    }

    @Override
    public @NotNull UUID[] getOwners(@NotNull AbstractHorseEntity entity) {
        UUID owner = entity.getOwnerUuid();

        if (owner == null) {
            return new UUID[0];
        } else {
            return new UUID[]{owner};
        }
    }

    @Override
    public boolean isOwner(@NotNull AbstractHorseEntity entity, @NotNull UUID owner) {
        return owner.equals(entity.getOwnerUuid());
    }

    @Override
    public boolean removeOwnership(@NotNull AbstractHorseEntity entity, @NotNull UUID owner) {
        entity.setTame(false);
        entity.setOwnerUuid(null);
        return true;
    }

    @Override
    public boolean transferOwnership(@NotNull AbstractHorseEntity entity, @NotNull UUID newOwner) {
        entity.setTame(true);
        entity.setOwnerUuid(newOwner);
        return true;
    }
}
