package dcp.mc.projectsavethepets.entities;

import dcp.mc.projectsavethepets.apis.NoteGeneratorApi;
import dcp.mc.projectsavethepets.apis.OwnershipApi;
import dcp.mc.projectsavethepets.apis.PetApi;
import java.util.UUID;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.nbt.NbtCompound;
import org.jetbrains.annotations.NotNull;

public final class Tamable implements NoteGeneratorApi<TameableEntity>, OwnershipApi<TameableEntity>, PetApi<TameableEntity> {
    public static final Tamable INSTANCE = new Tamable();

    private Tamable() {
    }

    @Override
    public void setupNbt(@NotNull TameableEntity entity, @NotNull NbtCompound nbt) {
        nbt.putBoolean("Sitting", false);
    }

    @Override
    public @NotNull Class<TameableEntity> type() {
        return TameableEntity.class;
    }

    @Override
    public boolean isPet(@NotNull TameableEntity entity) {
        return entity.isTamed();
    }

    @Override
    public UUID[] getOwners(@NotNull TameableEntity entity) {
        UUID owner = entity.getOwnerUuid();

        if (owner != null) {
            return new UUID[]{owner};
        } else {
            return new UUID[0];
        }
    }

    @Override
    public boolean isOwner(@NotNull TameableEntity entity, @NotNull UUID owner) {
        return owner.equals(entity.getOwnerUuid());
    }

    @Override
    public boolean removeOwnership(@NotNull TameableEntity entity, @NotNull UUID owner) {
        entity.setTamed(false);
        entity.setOwnerUuid(null);
        entity.setSitting(false);

        return true;
    }

    @Override
    public boolean transferOwnership(@NotNull TameableEntity entity, @NotNull UUID newOwner) {
        entity.setTamed(true);
        entity.setOwnerUuid(newOwner);

        return true;
    }
}
