package dcp.mc.projectsavethepets.entities;

import dcp.mc.projectsavethepets.apis.FriendlyFireApi;
import dcp.mc.projectsavethepets.apis.PetApi;
import java.util.UUID;
import net.minecraft.entity.player.PlayerEntity;
import org.jetbrains.annotations.NotNull;

public final class Player implements FriendlyFireApi<PlayerEntity>, PetApi<PlayerEntity> {
    public static final Player INSTANCE = new Player();

    private Player() {
    }

    @Override
    public @NotNull Class<PlayerEntity> type() {
        return PlayerEntity.class;
    }

    @Override
    public boolean isPet(@NotNull PlayerEntity entity) {
        return false;
    }

    @Override
    public UUID[] getOwners(@NotNull PlayerEntity entity) {
        return new UUID[]{entity.getUuid()};
    }

    @Override
    public boolean allowDamage(@NotNull PlayerEntity attacker, @NotNull PlayerEntity target) {
        return attacker.getUuid().equals(target.getUuid());
    }
}
