package dcp.mc.projectsavethepets.entities;

import dcp.mc.projectsavethepets.Utilities;
import dcp.mc.projectsavethepets.apis.NoteGeneratorApi;
import dcp.mc.projectsavethepets.apis.OwnershipApi;
import dcp.mc.projectsavethepets.apis.PetApi;
import dcp.mc.projectsavethepets.mixins.accessors.FoxEntityAccessor;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.passive.FoxEntity;
import net.minecraft.nbt.NbtCompound;
import org.jetbrains.annotations.NotNull;

public final class Fox implements NoteGeneratorApi<FoxEntity>, OwnershipApi<FoxEntity>, PetApi<FoxEntity> {
    public static final Fox INSTANCE = new Fox();

    private Fox() {
    }

    @Override
    public void setupNbt(@NotNull FoxEntity entity, @NotNull NbtCompound nbt) {
        nbt.putBoolean("Sleeping", false);
        nbt.putBoolean("Sitting", false);
        nbt.putBoolean("Crouching", false);
    }

    @Override
    public @NotNull Class<FoxEntity> type() {
        return FoxEntity.class;
    }

    @Override
    public boolean isPet(@NotNull FoxEntity entity) {
        return getOwners(entity).length > 0;
    }

    @Override
    public @NotNull UUID[] getOwners(@NotNull FoxEntity entity) {
        List<UUID> owners = ((FoxEntityAccessor) entity).getTrustedUuids();

        return owners.stream().filter(Objects::nonNull).toArray(UUID[]::new);
    }

    @Override
    public boolean isOwner(@NotNull FoxEntity entity, @NotNull UUID owner) {
        return ((FoxEntityAccessor) entity).getTrustedUuids().contains(owner);
    }

    @Override
    public boolean removeOwnership(@NotNull FoxEntity entity, @NotNull UUID owner) {
        if (owner.equals(Utilities.INSTANCE.getTrackableData(entity, FoxEntityAccessor.getOwner()).orElse(null))) {
            Utilities.INSTANCE.setTrackableData(entity, FoxEntityAccessor.getOwner(), Utilities.INSTANCE.getTrackableData(entity, FoxEntityAccessor.getOtherTrusted()));
        } else if (owner.equals(Utilities.INSTANCE.getTrackableData(entity, FoxEntityAccessor.getOwner()).orElse(null))) {
            Utilities.INSTANCE.setTrackableData(entity, FoxEntityAccessor.getOtherTrusted(), Optional.ofNullable(null));
        }

        return true;
    }

    @Override
    public boolean transferOwnership(@NotNull FoxEntity entity, @NotNull UUID newOwner) {
        if (isOwner(entity, newOwner)) {
            return true;
        }

        if (Utilities.INSTANCE.getTrackableData(entity, FoxEntityAccessor.getOwner()).orElse(null) == null) {
            Utilities.INSTANCE.setTrackableData(entity, FoxEntityAccessor.getOwner(), Optional.of(newOwner));
            return true;
        }

        Utilities.INSTANCE.setTrackableData(entity, FoxEntityAccessor.getOtherTrusted(), Optional.of(newOwner));

        return true;
    }
}
