package dcp.mc.projectsavethepets.config;

public final class OwnershipConfig {
    private final boolean removal;
    private final boolean transfer;
    private final boolean defaultShears;
    private final String[] extraShears;
    private final boolean defaultStick;
    private final String[] extraSticks;

    OwnershipConfig() {
        this.removal = true;
        this.transfer = false;
        this.defaultShears = true;
        this.extraShears = new String[0];
        this.defaultStick = true;
        this.extraSticks = new String[0];
    }

    public boolean isRemoval() {
        return removal;
    }

    public boolean isTransfer() {
        return transfer;
    }

    public boolean isDefaultShears() {
        return defaultShears;
    }

    public String[] getExtraShears() {
        return extraShears;
    }

    public String[] getExtraSticks() {
        return extraSticks;
    }

    public boolean isDefaultStick() {
        return defaultStick;
    }
}
