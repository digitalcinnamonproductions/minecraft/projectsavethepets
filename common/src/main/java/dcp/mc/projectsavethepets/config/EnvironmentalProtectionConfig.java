package dcp.mc.projectsavethepets.config;

public final class EnvironmentalProtectionConfig {
    private final boolean explosions;
    private final boolean bushes;
    private final boolean fires;
    private final boolean freezing;
    private final boolean drowning;

    EnvironmentalProtectionConfig() {
        this.explosions = false;
        this.bushes = false;
        this.fires = false;
        this.freezing = false;
        this.drowning = false;
    }

    public boolean isExplosions() {
        return explosions;
    }

    public boolean isBushes() {
        return bushes;
    }

    public boolean isFires() {
        return fires;
    }

    public boolean isFreezing() {
        return freezing;
    }

    public boolean isDrowning() {
        return drowning;
    }
}
