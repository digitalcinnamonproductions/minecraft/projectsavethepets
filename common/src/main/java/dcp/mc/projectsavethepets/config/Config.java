package dcp.mc.projectsavethepets.config;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mojang.authlib.GameProfileRepository;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import dcp.mc.projectsavethepets.Utilities;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Proxy;
import java.util.Arrays;

public final class Config {
    public static final Config INSTANCE;

    static {
        Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).setPrettyPrinting().create();
        File folder = Utilities.getConfigDirectory().toFile();
        File file = Utilities.getConfigDirectory().resolve("projectsavethepets.json").toFile();
        YggdrasilAuthenticationService authenticationService = new YggdrasilAuthenticationService(Proxy.NO_PROXY);
        GameProfileRepository profileRepository = authenticationService.createProfileRepository();
        MinecraftSessionService sessionService = authenticationService.createMinecraftSessionService();
        Config config;

        if (!folder.exists() && !folder.mkdirs()) {
            throw new ExceptionInInitializerError("Failed to create Configuration Folder");
        }

        if (!file.exists()) {
            config = new Config();
        } else {
            try (FileReader reader = new FileReader(file)) {
                config = gson.fromJson(reader, Config.class);
            } catch (IOException ignored) {
                config = new Config();
            }
        }

        config = new Config(config.damageProtection,
                config.environmentalProtection,
                config.statusEffectProtection,
                config.protectedEntities,
                config.revival,
                config.ownership,
                Arrays.stream(config.playerWhitelist).map((it) -> it.fillMissing(profileRepository, sessionService)).toArray(PlayerConfig[]::new));

        try (FileWriter writer = new FileWriter(file)) {
            writer.write(gson.toJson(config));
        } catch (IOException ignored) {
        }

        INSTANCE = config;
    }

    private final DamageProtectionConfig damageProtection;
    private final EnvironmentalProtectionConfig environmentalProtection;
    private final StatusEffectProtectionConfig statusEffectProtection;
    private final ProtectedEntitiesConfig protectedEntities;
    private final RevivalConfig revival;
    private final OwnershipConfig ownership;
    private final PlayerConfig[] playerWhitelist;

    private Config() {
        this(new DamageProtectionConfig(),
                new EnvironmentalProtectionConfig(),
                new StatusEffectProtectionConfig(),
                new ProtectedEntitiesConfig(),
                new RevivalConfig(),
                new OwnershipConfig(),
                new PlayerConfig[]{new PlayerConfig("SophiaCoxy", "a56db060-6840-4824-9a47-25f264ca3319", null)});
    }

    private Config(DamageProtectionConfig damageProtection, EnvironmentalProtectionConfig environmentalProtection, StatusEffectProtectionConfig statusEffectProtection, ProtectedEntitiesConfig protectedEntities, RevivalConfig revival, OwnershipConfig ownership, PlayerConfig[] playerWhitelist) {
        this.damageProtection = damageProtection;
        this.environmentalProtection = environmentalProtection;
        this.statusEffectProtection = statusEffectProtection;
        this.protectedEntities = protectedEntities;
        this.revival = revival;
        this.ownership = ownership;
        this.playerWhitelist = playerWhitelist;
    }

    public DamageProtectionConfig getDamageProtection() {
        return damageProtection;
    }

    public EnvironmentalProtectionConfig getEnvironmentalProtection() {
        return environmentalProtection;
    }

    public StatusEffectProtectionConfig getStatusEffectProtection() {
        return statusEffectProtection;
    }

    public ProtectedEntitiesConfig getProtectedEntities() {
        return protectedEntities;
    }

    public RevivalConfig getRevival() {
        return revival;
    }

    public OwnershipConfig getOwnership() {
        return ownership;
    }

    public PlayerConfig[] getPlayerWhitelist() {
        return playerWhitelist;
    }
}
