package dcp.mc.projectsavethepets.config;

public final class ProtectedEntitiesConfig {
    private final boolean tamable;
    private final boolean foxes;
    private final boolean horses;
    private final boolean players;

    private final String[] blacklist;

    ProtectedEntitiesConfig() {
        this.tamable = true;
        this.foxes = true;
        this.horses = true;
        this.players = false;
        this.blacklist = new String[0];
    }

    public boolean isTamable() {
        return tamable;
    }

    public boolean isFoxes() {
        return foxes;
    }

    public boolean isHorses() {
        return horses;
    }

    public boolean isPlayers() {
        return players;
    }

    public String[] getBlacklist() {
        return blacklist;
    }
}
