package dcp.mc.projectsavethepets.config;

public final class RevivalConfig {
    private final boolean enabled;
    private final boolean namedEntities;
    private final String[] revivalBlocks;
    private final String[] entityBlacklist;
    private final String[] entityWhitelist;

    RevivalConfig() {
        this.enabled = true;
        this.revivalBlocks = new String[]{"minecraft:copper_block"};
        this.entityBlacklist = new String[0];
        this.entityWhitelist = new String[0];
        this.namedEntities = true;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isEnabled() {
        return enabled;
    }

    public boolean isNamedEntities() {
        return namedEntities;
    }

    public String[] getRevivalBlocks() {
        return revivalBlocks;
    }

    public String[] getEntityBlacklist() {
        return entityBlacklist;
    }

    public String[] getEntityWhitelist() {
        return entityWhitelist;
    }
}
