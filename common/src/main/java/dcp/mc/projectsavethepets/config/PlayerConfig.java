package dcp.mc.projectsavethepets.config;

import com.mojang.authlib.Agent;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.GameProfileRepository;
import com.mojang.authlib.ProfileLookupCallback;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import net.minecraft.util.Uuids;

public final class PlayerConfig {
    private final String name;
    private final String onlineUuid;
    private final String offlineUuid;

    PlayerConfig() {
        this(null, null, null);
    }

    PlayerConfig(String name, String onlineUuid, String offlineUuid) {
        this.name = name;
        this.onlineUuid = onlineUuid;
        this.offlineUuid = offlineUuid;
    }

    public boolean uuidEqual(String uuid) {
        return uuid.equals(onlineUuid) || uuid.equals(offlineUuid);
    }

    public PlayerConfig fillMissing(GameProfileRepository profileRepository, MinecraftSessionService sessionService) {
        if (name == null && onlineUuid == null && offlineUuid == null) {
            throw new IllegalStateException("name, online_uuid, and offline_uuid cannot all be null");
        }

        String newName = name;
        String newOnlineUuid = onlineUuid;
        String newOfflineUuid = offlineUuid;

        if (newOnlineUuid == null && newName != null) {
            AtomicReference<String> uuid = new AtomicReference<>(null);

            profileRepository.findProfilesByNames(new String[]{newName}, Agent.MINECRAFT, new ProfileLookupCallback() {
                @Override
                public void onProfileLookupSucceeded(GameProfile profile) {
                    uuid.set(profile.getId().toString());
                }

                @Override
                public void onProfileLookupFailed(GameProfile profile, Exception exception) {

                }
            });

            newOnlineUuid = uuid.get();
        }

        if (newName == null && newOnlineUuid != null) {
            newName = sessionService.fillProfileProperties(new GameProfile(UUID.fromString(newOnlineUuid), null), false).getName();
        }

        if (newName != null) {
            newOfflineUuid = Uuids.getOfflinePlayerUuid(newName).toString();
        }

        return new PlayerConfig(newName, newOnlineUuid, newOfflineUuid);
    }
}
