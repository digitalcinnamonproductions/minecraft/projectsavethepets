package dcp.mc.projectsavethepets.config;

public final class DamageProtectionConfig {
    private final boolean projectiles;
    private final boolean sweeping;
    private final boolean direct;
    private final boolean explosions;
    private final boolean vanillaTeams;

    DamageProtectionConfig() {
        this.projectiles = true;
        this.sweeping = true;
        this.direct = true;
        this.explosions = true;
        this.vanillaTeams = false;
    }

    public boolean isProjectiles() {
        return projectiles;
    }

    public boolean isSweeping() {
        return sweeping;
    }

    public boolean isDirect() {
        return direct;
    }

    public boolean isExplosions() {
        return explosions;
    }

    public boolean isVanillaTeams() {
        return vanillaTeams;
    }
}
