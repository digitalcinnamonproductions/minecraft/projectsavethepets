package dcp.mc.projectsavethepets.config;

public final class StatusEffectProtectionConfig {
    private final boolean enabled;
    private final String[] livingEffects;
    private final String[] undeadEffects;

    StatusEffectProtectionConfig() {
        this.enabled = true;
        this.livingEffects = new String[]{"minecraft:instant_damage", "minecraft:poison"};
        this.undeadEffects = new String[]{"minecraft:instant_health", "minecraft:regeneration"};
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isEnabled() {
        return enabled;
    }

    public String[] getLivingEffects() {
        return livingEffects;
    }

    public String[] getUndeadEffects() {
        return undeadEffects;
    }
}
