package dcp.mc.projectsavethepets.notes;

import dcp.mc.projectsavethepets.apis.NoteGeneratorApi;
import net.minecraft.entity.passive.WolfEntity;
import net.minecraft.nbt.NbtCompound;
import org.jetbrains.annotations.NotNull;

public final class Wolf implements NoteGeneratorApi<WolfEntity> {
    public static final Wolf INSTANCE = new Wolf();

    private Wolf() {
    }

    @Override
    public @NotNull Class<WolfEntity> type() {
        return WolfEntity.class;
    }

    @Override
    public void setupNbt(@NotNull WolfEntity entity, @NotNull NbtCompound nbt) {
        nbt.remove("AngerTime");
        nbt.remove("AngryAt");
    }
}
