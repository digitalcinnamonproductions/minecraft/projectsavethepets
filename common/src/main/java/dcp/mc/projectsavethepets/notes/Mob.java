package dcp.mc.projectsavethepets.notes;

import dcp.mc.projectsavethepets.apis.NoteGeneratorApi;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.nbt.NbtCompound;
import org.jetbrains.annotations.NotNull;

public final class Mob implements NoteGeneratorApi<MobEntity> {
    public static final Mob INSTANCE = new Mob();

    private Mob() {
    }

    @Override
    public @NotNull Class<MobEntity> type() {
        return MobEntity.class;
    }

    @Override
    public void setupNbt(@NotNull MobEntity entity, @NotNull NbtCompound nbt) {
        nbt.remove("HandItems");
        nbt.remove("ArmorItems");
        nbt.remove("Leash");
    }
}
