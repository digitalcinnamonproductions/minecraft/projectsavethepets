package dcp.mc.projectsavethepets.notes;


import dcp.mc.projectsavethepets.apis.NoteGeneratorApi;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.NbtCompound;
import org.jetbrains.annotations.NotNull;

public final class Living implements NoteGeneratorApi<LivingEntity> {
    public static final Living INSTANCE = new Living();

    private Living() {
    }

    @Override
    public @NotNull Class<LivingEntity> type() {
        return LivingEntity.class;
    }

    @Override
    public void setupNbt(@NotNull LivingEntity entity, @NotNull NbtCompound nbt) {
        nbt.remove("Health");
        nbt.remove("HurtTime");
        nbt.remove("HurtByTimestamp");
        nbt.remove("DeathTime");
        nbt.remove("FallFlying");
        nbt.remove("SleepingX");
        nbt.remove("SleepingY");
        nbt.remove("SleepingZ");
        nbt.remove("Brain");
        nbt.remove("AbsorptionAmount");
        nbt.remove("ActiveEffects");
    }
}
