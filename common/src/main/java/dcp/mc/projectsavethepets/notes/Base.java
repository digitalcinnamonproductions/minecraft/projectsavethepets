package dcp.mc.projectsavethepets.notes;


import dcp.mc.projectsavethepets.apis.NoteGeneratorApi;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NbtCompound;
import org.jetbrains.annotations.NotNull;

public final class Base implements NoteGeneratorApi<Entity> {
    public static final Base INSTANCE = new Base();

    private Base() {
    }

    @Override
    public @NotNull Class<Entity> type() {
        return Entity.class;
    }

    @Override
    public void setupNbt(@NotNull Entity entity, @NotNull NbtCompound nbt) {
        nbt.remove("Pos");
        nbt.remove("Motion");
        nbt.remove("Rotation");
        nbt.remove("FallDistance");
        nbt.remove("Fire");
        nbt.remove("Air");
        nbt.remove("OnGround");
        nbt.remove("PortalCooldown");
        nbt.remove("Passengers");
    }
}
