package dcp.mc.projectsavethepets.apis;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NbtCompound;
import org.jetbrains.annotations.NotNull;

public interface NoteGeneratorApi<T extends Entity> {
    @NotNull Class<T> type();

    @SuppressWarnings("unused")
    void setupNbt(@NotNull T entity, @NotNull NbtCompound nbt);
}
