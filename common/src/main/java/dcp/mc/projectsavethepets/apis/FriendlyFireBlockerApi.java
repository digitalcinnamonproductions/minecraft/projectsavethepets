package dcp.mc.projectsavethepets.apis;

import java.util.UUID;
import net.minecraft.entity.player.PlayerEntity;
import org.jetbrains.annotations.NotNull;

public interface FriendlyFireBlockerApi {
    boolean preventDamage(@NotNull PlayerEntity attacker, @NotNull UUID owner);
}
