package dcp.mc.projectsavethepets.apis;

import java.util.UUID;
import net.minecraft.entity.Entity;
import org.jetbrains.annotations.NotNull;

public interface OwnershipApi<T extends Entity> {
    @NotNull Class<T> type();

    boolean isOwner(@NotNull T entity, @NotNull UUID owner);

    @SuppressWarnings("SameReturnValue")
    boolean removeOwnership(@NotNull T entity, @NotNull UUID owner);

    @SuppressWarnings("SameReturnValue")
    boolean transferOwnership(@NotNull T entity, @NotNull UUID newOwner);
}
