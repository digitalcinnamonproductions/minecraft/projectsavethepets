package dcp.mc.projectsavethepets.apis;

import java.util.UUID;
import net.minecraft.entity.player.PlayerEntity;
import org.jetbrains.annotations.NotNull;

public interface TransferBlockerApi {
    boolean preventTransfer(@NotNull PlayerEntity attacker, @NotNull UUID owner);
}
