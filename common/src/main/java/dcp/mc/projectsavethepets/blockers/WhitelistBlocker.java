package dcp.mc.projectsavethepets.blockers;

import dcp.mc.projectsavethepets.apis.FriendlyFireBlockerApi;
import dcp.mc.projectsavethepets.apis.TransferBlockerApi;
import dcp.mc.projectsavethepets.config.Config;
import dcp.mc.projectsavethepets.config.PlayerConfig;
import java.util.UUID;
import net.minecraft.entity.player.PlayerEntity;
import org.jetbrains.annotations.NotNull;

public final class WhitelistBlocker implements FriendlyFireBlockerApi, TransferBlockerApi {
    public static final WhitelistBlocker INSTANCE = new WhitelistBlocker();

    private WhitelistBlocker() {
    }

    @Override
    public boolean preventDamage(@NotNull PlayerEntity attacker, @NotNull UUID owner) {
        String uuid = owner.toString();

        for (PlayerConfig config : Config.INSTANCE.getPlayerWhitelist()) {
            if (config.uuidEqual(uuid)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean preventTransfer(@NotNull PlayerEntity attacker, @NotNull UUID owner) {
        return preventDamage(attacker, owner);
    }
}
