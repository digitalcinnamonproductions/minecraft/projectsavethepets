package dcp.mc.projectsavethepets.blockers;

import com.mojang.authlib.GameProfile;
import dcp.mc.projectsavethepets.apis.FriendlyFireBlockerApi;
import java.util.UUID;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.scoreboard.AbstractTeam;
import org.jetbrains.annotations.NotNull;

public final class VanillaTeamBlocker implements FriendlyFireBlockerApi {
    public static final VanillaTeamBlocker INSTANCE = new VanillaTeamBlocker();

    private VanillaTeamBlocker() {
    }

    @Override
    public boolean preventDamage(@NotNull PlayerEntity attacker, @NotNull UUID owner) {
        AbstractTeam team = attacker.getScoreboardTeam();

        if (team == null) return false;
        if (team.isFriendlyFireAllowed()) return false;

        GameProfile profile;

        if (attacker.getWorld().isClient) {
            // TODO: Find alternative that does not require HTTP
            profile = MinecraftClient.getInstance().getSessionService().fillProfileProperties(new GameProfile(owner, null), false);
        } else {
            assert attacker.getServer() != null;
            profile = attacker.getServer().getUserCache().getByUuid(owner).orElse(null);
        }

        if (profile == null) return false;

        return team.getPlayerList().contains(profile.getName());
    }
}
