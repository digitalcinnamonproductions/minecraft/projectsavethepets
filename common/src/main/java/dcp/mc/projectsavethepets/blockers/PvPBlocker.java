package dcp.mc.projectsavethepets.blockers;

import dcp.mc.projectsavethepets.apis.FriendlyFireBlockerApi;
import java.util.UUID;
import net.minecraft.entity.player.PlayerEntity;
import org.jetbrains.annotations.NotNull;

public final class PvPBlocker implements FriendlyFireBlockerApi {
    public static final PvPBlocker INSTANCE = new PvPBlocker();

    private PvPBlocker() {
    }

    @Override
    public boolean preventDamage(@NotNull PlayerEntity attacker, @NotNull UUID owner) {
        return attacker.getServer() != null && !attacker.getServer().isPvpEnabled();
    }
}
