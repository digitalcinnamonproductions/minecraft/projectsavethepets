package dcp.mc.projectsavethepets.mixins.effects;

import dcp.mc.projectsavethepets.ProjectSaveThePets;
import dcp.mc.projectsavethepets.Utilities;
import dcp.mc.projectsavethepets.config.Config;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.registry.Registries;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(StatusEffect.class)
final class Instant {
    @Inject(method = "applyInstantEffect", at = @At(value = "HEAD"), cancellable = true)
    private void preventEffect(Entity source, Entity attacker, LivingEntity target, int amplifier, double proximity, CallbackInfo ci) {
        if (!Config.INSTANCE.getStatusEffectProtection().isEnabled()) {
            return;
        }

        if (!(attacker instanceof PlayerEntity player)) {
            return;
        }

        if (!ProjectSaveThePets.INSTANCE.isFriendly(player, target)) {
            return;
        }

        StatusEffect instance = (StatusEffect) (Object) this;
        String[] effects = target.isUndead() ?
                Config.INSTANCE.getStatusEffectProtection().getUndeadEffects() :
                Config.INSTANCE.getStatusEffectProtection().getLivingEffects();

        for (String effect : effects) {
            if (instance.equals(Registries.STATUS_EFFECT.get(Utilities.INSTANCE.convertToId(effect)))) {
                ci.cancel();
                return;
            }
        }
    }
}
