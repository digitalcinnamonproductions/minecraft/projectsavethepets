package dcp.mc.projectsavethepets.mixins.protection;

import dcp.mc.projectsavethepets.ProjectSaveThePets;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ServerPlayerEntity.class)
final class DirectServer {
    @Inject(method = "attack", at = @At("HEAD"), cancellable = true)
    private void preventAttack(Entity target, CallbackInfo callbackInfo) {
        PlayerEntity attacker = (PlayerEntity) (Object) this;

        if (ProjectSaveThePets.INSTANCE.isFriendly(attacker, target)) {
            callbackInfo.cancel();
        }
    }
}
