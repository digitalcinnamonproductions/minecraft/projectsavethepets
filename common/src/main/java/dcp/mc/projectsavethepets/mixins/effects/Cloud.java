package dcp.mc.projectsavethepets.mixins.effects;

import dcp.mc.projectsavethepets.ProjectSaveThePets;
import dcp.mc.projectsavethepets.Utilities;
import dcp.mc.projectsavethepets.config.Config;
import net.minecraft.entity.AreaEffectCloudEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.registry.Registries;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(value = LivingEntity.class, priority = 1500)
abstract class Cloud {
    private Cloud() {
        throw new AssertionError();
    }

    @Shadow
    public abstract boolean isUndead();

    @Inject(method = "addStatusEffect(Lnet/minecraft/entity/effect/StatusEffectInstance;Lnet/minecraft/entity/Entity;)Z", at = @At(value = "HEAD"), cancellable = true)
    private void preventEffect(StatusEffectInstance instance, Entity source, CallbackInfoReturnable<Boolean> cir) {
        if (!(source instanceof AreaEffectCloudEntity areaEffectCloud)) {
            return;
        }

        if (!Config.INSTANCE.getStatusEffectProtection().isEnabled()) {
            return;
        }

        if (!(areaEffectCloud.getOwner() instanceof PlayerEntity player)) {
            return;
        }

        if (!ProjectSaveThePets.INSTANCE.isFriendly(player, (LivingEntity) (Object) this)) {
            return;
        }

        String[] effects = this.isUndead() ?
                Config.INSTANCE.getStatusEffectProtection().getUndeadEffects() :
                Config.INSTANCE.getStatusEffectProtection().getLivingEffects();

        for (String effect : effects) {
            if (instance.getEffectType().equals(Registries.STATUS_EFFECT.get(Utilities.INSTANCE.convertToId(effect)))) {
                cir.setReturnValue(false);
                return;
            }
        }
    }
}
