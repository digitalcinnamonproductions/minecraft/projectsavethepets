package dcp.mc.projectsavethepets.mixins.protection;

import dcp.mc.projectsavethepets.ProjectSaveThePets;
import dcp.mc.projectsavethepets.config.Config;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ProjectileEntity.class)
abstract class Projectile {
    @Shadow
    public @Nullable
    abstract Entity getOwner();

    @Inject(method = "canHit", at = @At(value = "HEAD"), cancellable = true)
    private void preventCollision(Entity target, CallbackInfoReturnable<Boolean> cir) {
        if (!Config.INSTANCE.getDamageProtection().isProjectiles()) {
            return;
        }

        Entity attacker = this.getOwner();

        if (!(attacker instanceof PlayerEntity player)) {
            return;
        }

        if (ProjectSaveThePets.INSTANCE.isFriendly(player, target)) {
            cir.setReturnValue(false);
        }
    }
}
