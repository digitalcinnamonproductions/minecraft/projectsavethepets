package dcp.mc.projectsavethepets.mixins.protection;

import dcp.mc.projectsavethepets.ProjectSaveThePets;
import dcp.mc.projectsavethepets.config.Config;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(PlayerEntity.class)
final class Sweeping {
    @Redirect(method = "attack", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/player/PlayerEntity;isTeammate(Lnet/minecraft/entity/Entity;)Z"))
    private boolean preventAttack(PlayerEntity player, Entity target) {
        if (!isProtected(player, target)) {
            return player.isTeammate(target);
        }

        return true;
    }

    private boolean isProtected(PlayerEntity player, Entity target) {
        if (!Config.INSTANCE.getDamageProtection().isSweeping()) {
            return false;
        }

        return ProjectSaveThePets.INSTANCE.isFriendly(player, target);
    }
}
