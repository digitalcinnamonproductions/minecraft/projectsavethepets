package dcp.mc.projectsavethepets.mixins.protection;

import dcp.mc.projectsavethepets.ProjectSaveThePets;
import dcp.mc.projectsavethepets.config.Config;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.explosion.Explosion;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(Explosion.class)
abstract class Explosions {
    private Explosions() {
        throw new AssertionError();
    }

    @Shadow
    @Nullable
    public abstract LivingEntity getCausingEntity();

    @Redirect(method = "collectBlocksAndDamageEntities", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/Entity;squaredDistanceTo(Lnet/minecraft/util/math/Vec3d;)D"))
    private double preventExplosionDamage(Entity instance, Vec3d vector) {
        if (isProtected(instance)) {
            return Double.MAX_VALUE;
        } else {
            return instance.squaredDistanceTo(vector);
        }
    }

    private boolean isProtected(Entity instance) {
        if (!Config.INSTANCE.getDamageProtection().isExplosions()) {
            return false;
        }

        if (!(getCausingEntity() instanceof PlayerEntity player)) {
            return false;
        }

        return ProjectSaveThePets.INSTANCE.isFriendly(player, instance);
    }
}
