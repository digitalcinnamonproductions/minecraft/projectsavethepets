package dcp.mc.projectsavethepets.mixins.protection;

import dcp.mc.projectsavethepets.ProjectSaveThePets;
import dcp.mc.projectsavethepets.Utilities;
import dcp.mc.projectsavethepets.config.Config;
import dcp.mc.projectsavethepets.mixins.accessors.KeyBindingAccessor;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;


@Mixin(ClientPlayerInteractionManager.class)
final class DirectClient {
    @Inject(method = "attackEntity", at = @At(value = "HEAD"), cancellable = true)
    private void preventAttack(PlayerEntity player, Entity target, CallbackInfo callbackInfo) {
        if (!Config.INSTANCE.getDamageProtection().isDirect()) {
            return;
        }

        if (isKeyDown(player)) {
            return;
        }

        if (ProjectSaveThePets.INSTANCE.isFriendly(player, target)) {
            callbackInfo.cancel();
        }
    }

    private boolean isKeyDown(PlayerEntity player) {
        KeyBinding allowDamageKeybinding = Utilities.getAllowDamageKeybinding();
        InputUtil.Key sneakKey = ((KeyBindingAccessor) MinecraftClient.getInstance().options.sneakKey).getBoundKey();
        InputUtil.Key allowDamageKey = ((KeyBindingAccessor) allowDamageKeybinding).getBoundKey();

        return sneakKey.getCode() == allowDamageKey.getCode() &&
                sneakKey.getCategory() == allowDamageKey.getCategory() &&
                player.isSneaking() ||
                allowDamageKeybinding.isPressed();
    }
}
