package dcp.mc.projectsavethepets.fabric;

import dcp.mc.projectsavethepets.ProjectSaveThePets;
import dcp.mc.projectsavethepets.apis.FriendlyFireApi;
import dcp.mc.projectsavethepets.apis.FriendlyFireBlockerApi;
import dcp.mc.projectsavethepets.apis.NoteGeneratorApi;
import dcp.mc.projectsavethepets.apis.OwnershipApi;
import dcp.mc.projectsavethepets.apis.PetApi;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;

@SuppressWarnings("unused")
public final class ModInitImpl implements ModInitializer {
    @Override
    public void onInitialize() {
        FabricLoader.getInstance()
                .getEntrypoints("projectsavethepets-ffa", FriendlyFireApi.class)
                .forEach(ProjectSaveThePets.INSTANCE.FRIENDLY_FIRE_APIS::add);

        ProjectSaveThePets.INSTANCE.FRIENDLY_FIRE_BLOCKER_APIS.addAll(FabricLoader.getInstance()
                .getEntrypoints("projectsavethepets-ffba", FriendlyFireBlockerApi.class));

        FabricLoader.getInstance()
                .getEntrypoints("projectsavethepets-oa", OwnershipApi.class)
                .forEach(ProjectSaveThePets.INSTANCE.OWNERSHIP_APIS::add);

        FabricLoader.getInstance()
                .getEntrypoints("projectsavethepets-nga", NoteGeneratorApi.class)
                .forEach(ProjectSaveThePets.INSTANCE.NOTE_GENERATOR_APIS::add);

        FabricLoader.getInstance()
                .getEntrypoints("projectsavethepets-pa", PetApi.class)
                .forEach(ProjectSaveThePets.INSTANCE.PET_APIS::add);
    }
}
