package dcp.mc.projectsavethepets.fabric;

import java.nio.file.Path;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.option.KeyBinding;

public class UtilitiesImpl {
    public static KeyBinding getAllowDamageKeybinding() {
        return ClientInitImpl.getAllowDamageKeyBinding();
    }

    public static Path getConfigDirectory() {
        return FabricLoader.getInstance().getConfigDir();
    }
}
