package dcp.mc.projectsavethepets.forge;

import dcp.mc.projectsavethepets.ProjectSaveThePets;
import dcp.mc.projectsavethepets.apis.FriendlyFireApi;
import dcp.mc.projectsavethepets.apis.FriendlyFireBlockerApi;
import dcp.mc.projectsavethepets.apis.NoteGeneratorApi;
import dcp.mc.projectsavethepets.apis.OwnershipApi;
import dcp.mc.projectsavethepets.apis.PetApi;
import net.minecraftforge.fml.IExtensionPoint;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.network.NetworkConstants;

@Mod("projectsavethepets")
public final class ModInitImpl {
    public ModInitImpl() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onInterModProcessEvent);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(ClientInitImpl::onRegisterKeyMappingsEvent);
        ModLoadingContext.get().registerExtensionPoint(IExtensionPoint.DisplayTest.class, () -> new IExtensionPoint.DisplayTest(() -> NetworkConstants.IGNORESERVERONLY, (a, b) -> true));
    }

    private void onInterModProcessEvent(InterModProcessEvent event) {
        InterModComms.getMessages("projectsavethepets").forEach(imcMessage -> {
            Object object = imcMessage.messageSupplier().get();

            if (object instanceof FriendlyFireApi api) {
                ProjectSaveThePets.INSTANCE.FRIENDLY_FIRE_APIS.add(api);
            }

            if (object instanceof FriendlyFireBlockerApi api) {
                ProjectSaveThePets.INSTANCE.FRIENDLY_FIRE_BLOCKER_APIS.add(api);
            }

            if (object instanceof OwnershipApi api) {
                ProjectSaveThePets.INSTANCE.OWNERSHIP_APIS.add(api);
            }

            if (object instanceof NoteGeneratorApi api) {
                ProjectSaveThePets.INSTANCE.NOTE_GENERATOR_APIS.add(api);
            }

            if (object instanceof PetApi api) {
                ProjectSaveThePets.INSTANCE.PET_APIS.add(api);
            }
        });
    }
}
