## Config for Project: Save the Pets!

**File:** `config/projectsavethepets.json`

```json5
{
  // Protection from friendly fire sources
  "damage_protection": {
    // Protection from Arrows and similar projectiles
    "projectiles": true,
    // Protection from the sweeping effect on swords
    "sweeping": true,
    // Protection from a direct attack with or without a weapon
    "direct": true,
    // Protection from sources like TNT that you lit yourself
    "explosions": true,
    // Should we protect teammate pets?
    "vanilla_teams": false
  },
  // Protection from Environmental sources
  "environmental_protection": {
    // Protection from all explosions, including Creepers
    "explosions": false,
    // Protection from Sweet Berry Bushes
    "bushes": false,
    // Protection from Fire and Lava
    "fires": false,
    // Protection from freezing (EG: Powder Snow)
    "freezing": false,
    // Protection from drowning (Unlimited Air)
    "drowning": false
  },
  // Protection from status effects from potions
  "status_effect_protection": {
    // Should this feature be enabled?
    "enabled": true,
    // Effects to block from Living Entities
    "living_effects": [
      "minecraft:instant_damage",
      "minecraft:poison"
    ],
    // Effects to block from Undead Entities
    "undead_effects": [
      "minecraft:instant_health",
      "minecraft:regeneration"
    ]
  },
  // Entities that are protected
  "protected_entities": {
    // Protect Wolves, Parrots, Cats from damage
    "tamable": true,
    // Protect Foxes from damage
    "foxes": true,
    // Protect Horses, Donkeys, and similar from damage
    "horses": true,
    // Protect Players - Might get you banned from servers.
    "players": false,
    // Entities that should not receive protection
    "blacklist": []
  },
  // Revival Feature configuration
  "revival": {
    // Should the feature be enabled?
    "enabled": true,
    // Allowed entities with a custom name to drop a note
    "named_entities": true,
    // Blocks used to revive an entity (Destroyed on use!)
    "revival_blocks": [
      "minecraft:copper_block"
    ],
    // Entities that should not be able to be revived
    "entity_blacklist": [],
    // Entities that should always drop a note
    "entity_whitelist": []
  },
  // Configuration about Ownership
  "ownership": {
    // Should players be able to remove themselves as the owner?
    "removal": true,
    // Should players be able to assume ownership?
    "transfer": false,
    // Should the default shears (And modded shears) be usable to remove ownership?
    "default_shears": true,
    // Items that should remove ownership
    "extra_shears": [],
    // Should the default stick be usable to assume ownership?
    "default_stick": true,
    // Items that should assume ownership
    "extra_sticks": []
  },
  // Players listed here are protected!
  "player_whitelist": [
    {
      // The name of the account. Can be null/left out
      "name": "SophiaCoxy",
      // The UUID of the account. Can be null/left out
      "online_uuid": "a56db060-6840-4824-9a47-25f264ca3319",
      // The Offline UUID of the account. Usually generated automatically
      // Can be null/left out
      "offline_uuid": null
    }
  ]
}
```
