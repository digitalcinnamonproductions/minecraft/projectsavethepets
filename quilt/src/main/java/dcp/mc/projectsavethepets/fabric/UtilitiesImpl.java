package dcp.mc.projectsavethepets.fabric;

import java.nio.file.Path;
import net.minecraft.client.option.KeyBinding;
import org.quiltmc.loader.api.QuiltLoader;

public class UtilitiesImpl {
    public static KeyBinding getAllowDamageKeybinding() {
        return ClientInitImpl.getAllowDamageKeyBinding();
    }

    public static Path getConfigDirectory() {
        return QuiltLoader.getConfigDir();
    }
}
