package dcp.mc.projectsavethepets.fabric;

import dcp.mc.projectsavethepets.ProjectSaveThePets;
import dcp.mc.projectsavethepets.apis.FriendlyFireApi;
import dcp.mc.projectsavethepets.apis.FriendlyFireBlockerApi;
import dcp.mc.projectsavethepets.apis.NoteGeneratorApi;
import dcp.mc.projectsavethepets.apis.OwnershipApi;
import dcp.mc.projectsavethepets.apis.PetApi;
import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.loader.api.QuiltLoader;
import org.quiltmc.qsl.base.api.entrypoint.ModInitializer;

@SuppressWarnings("unused")
public final class ModInitImpl implements ModInitializer {
    @Override
    public void onInitialize(ModContainer modContainer) {
        QuiltLoader.getEntrypoints("projectsavethepets-ffa", FriendlyFireApi.class)
                .forEach(ProjectSaveThePets.INSTANCE.FRIENDLY_FIRE_APIS::add);

        ProjectSaveThePets.INSTANCE.FRIENDLY_FIRE_BLOCKER_APIS.addAll(QuiltLoader
                .getEntrypoints("projectsavethepets-ffba", FriendlyFireBlockerApi.class));

        QuiltLoader.getEntrypoints("projectsavethepets-oa", OwnershipApi.class)
                .forEach(ProjectSaveThePets.INSTANCE.OWNERSHIP_APIS::add);

        QuiltLoader.getEntrypoints("projectsavethepets-nga", NoteGeneratorApi.class)
                .forEach(ProjectSaveThePets.INSTANCE.NOTE_GENERATOR_APIS::add);

        QuiltLoader.getEntrypoints("projectsavethepets-pa", PetApi.class)
                .forEach(ProjectSaveThePets.INSTANCE.PET_APIS::add);
    }
}
